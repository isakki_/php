<?php

$conn = mysqli_connect('localhost', 'root', '', 'clg');

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>
<body>
    <form action="update.php" method="post">
        <section class="vh-100">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="card bg-dark text-white" style="border-radius: 1rem;">
                            <div class="card-body p-5 text-center">
                                <?php
                                if (isset($_POST['update'])) {
                                    $username = $_POST['username'];
                                    $email = $_POST['email'];
                                    $user = $_POST['user'];
                                    $query = "UPDATE clg_update SET username = '$username' , email = '$email' WHERE username = '$user'";
                                    $res  = mysqli_query($conn, $query);
                                }
                                ?>
                                <div class="mb-md-5 mt-md-4 pb-5">
                                    <h2 class="fw-bold mb-2 pb-3 text-uppercase">Update</h2>
                                    <div class="form-outline form-white mb-4">
                                        <input type="text" id="typeEmailX" class="form-control form-control-lg" placeholder="Username" name="username" />
                                    </div>
                                    <div class="form-outline form-white mb-4">
                                        <input type="email" id="typePasswordX" class="form-control form-control-lg" name="email" placeholder="Email" />
                                    </div>
                                    <button class="btn btn-outline-light btn-lg px-5" type="submit" name="update">UPDATE</button> <br> <br>
                                    <select name="user" id="">
                                        <?php
                                        $query = "SELECT * FROM clg_update";
                                        $result = mysqli_query($conn, $query);
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            $user = $row['username'];
                                            echo "<option value='$user'>$user</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>