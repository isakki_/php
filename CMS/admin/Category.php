<?php ob_start();
include 'includes/db.php'; 
include 'includes/header.php';
include 'includes/sidebar.php';
include 'includes/nav.php'; 
?>


<?php
if(isset($_POST['submit'])){
    $add_cat_title = $_POST['cat_title'];

    if(empty($add_cat_title)){
        echo'<div class="alert alert-danger container" role="alert">
                This Field Cannot be Empty!
             </div>';
    }
    else{
    $add_cat_query = "INSERT INTO category(category_title) VALUES ('$add_cat_title')";
    mysqli_query($connection , $add_cat_query);
    }
}
?>

<div class="container-fluid">
    
<div class="row">

<!-- Area Chart -->
<div class="col-xl-6 col-lg-6">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Add categories</h6>
        </div>
        <div class="card-body">
            <form action="category.php" method="post">
                <input type="text" class="form-control" name="cat_title"><br>
                <input type="submit" class="btn btn-outline-primary" value="submit" name="submit">
            </form>
        </div>
    </div>
</div>


<div class="col-xl-6 col-lg-6">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">All categories</h6>
            <div class="dropdown no-arrow">
        </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Categories</th>
                        <th>Remove</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>

                    <?php

                    $select_all_query = "SELECT * FROM category";
                    $select_all_result = mysqli_query($connection , $select_all_query);

                    while($row = mysqli_fetch_assoc($select_all_result)){
                        $cat_title = $row['category_title'];
                        $cat_id = $row['category_id'];

                        echo "<tr>
                        <td>{$cat_title}</td>
                        <td><a href='category.php?delete={$cat_id}' class='btn btn-outline-danger'>Delete</a></td>
                        <td><a href='category.php?edit={$cat_id}' class='btn btn-outline-warning'>Edit</a></td>
                        </tr>";
                    }

                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
                                    // category edit 
        
            if(isset($_GET['edit'])){
                $edit_cat_id = $_GET['edit'];
                
                $select_all_query = "SELECT * FROM category WHERE category_id = $edit_cat_id";
                $select_all_result = mysqli_query($connection , $select_all_query);

                while($row = mysqli_fetch_assoc($select_all_result)){
                    $cat_title = $row['category_title'];
                    $cat_id = $row['category_id'];

            ?>
                <!-- Edit category -->
</div>
<div class="col-xl-6 col-lg-6">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Edit categories</h6>
        </div>
        <div class="card-body">
            <form action="category.php?id=<?php echo $cat_id?>" method="post">
                <input type="text" class="form-control" name="cat_title" value="<?php echo $cat_title;?>"><br>
                <input type="submit" class="btn btn-primary" value="update" name="update">
            </form>
        </div>
    </div>
</div>
<?php

        }  
        // while loop cloes
    }
    // if close
?>

<?php
                            // category delete 

    if(isset($_GET['delete'])){
    $cat_delete_id = $_GET['delete'];
    $delete_cat_query = "DELETE FROM category WHERE category_id = $cat_delete_id";
    $delete_query = mysqli_query($connection , $delete_cat_query);
    header('location: Category.php');
    }
?>



                <!-- category update  -->
<?php
    if(isset($_POST['update'])){
        $update_cat_title = $_POST['cat_title'];
        $update_cat_id = $_GET['id'];

        $update_cat_query = "UPDATE category SET category_title = '$update_cat_title' WHERE category_id = $update_cat_id";
        $update_result = mysqli_query($connection , $update_cat_query);

        header('location: Category.php');
    }
?>


<?php 
include 'includes/footer.php'
?>










