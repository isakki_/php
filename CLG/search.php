<?php

$conn = mysqli_connect('localhost', 'root', '', 'clgs');

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>

<body>
    <form action="search.php" method="post">
        <section class="vh-100">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="card bg-dark text-white" style="border-radius: 1rem;">
                            <div class="card-body p-5 text-center">
                                <select name="user" id="">
                                    <?php
                                    $query = "SELECT * FROM student";
                                    $result = mysqli_query($conn, $query);
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        $user = $row['username'];
                                        echo "<option value='$user'>$user</option>";
                                    }
                                    ?>
                                </select> <br> <br>
                                <button class="btn btn-outline-light btn-lg px-5" type="submit" name="update">search</button>
                            </div> <br> <br>
                            <?php
                            if (isset($_POST['update'])) {
                                $username = $_POST['user'];
                                $all_query = "SELECT * FROM student WHERE username = '$username' ";
                                $res = mysqli_query($conn, $all_query);
                                while ($rows = mysqli_fetch_assoc($res)) {
                            ?>
                                    <label for="">
                                        <?php echo $rows['username']; ?>
                                    </label> <br>
                                    <label for="">
                                        <?php echo $rows['email']; ?>
                                    </label>
                                    <label for="">
                                        <?php echo $rows['dob']; ?>
                                    </label>
                                    <label for="">
                                        <?php echo $rows['reg_no']; ?>
                                    </label>
                                    <label for="">
                                        <?php echo $rows['phone_no']; ?>
                                    </label>
                            <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>