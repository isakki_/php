<?php
include 'includes/db.php';
include 'includes/header.php';
?>


<div class="div">


        <!-- Page content-->
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-8">
                    <!-- Post content-->

                    <?php
                    $post_query = "SELECT * FROM posts WHERE post_status = 'Published'";
                    $post_result = mysqli_query($connection , $post_query);

                    while($row = mysqli_fetch_assoc($post_result)){
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_content = mb_strimwidth($row['post_content'] , 0 , 47 , "...");
                        $post_image = $row['post_image'];
                        $post_tag = $row['post_tags'];
                        $post_id = $row['post_id'];
                    ?>
                    
                    <article>
                        <!-- Post header-->
                        <header class="mb-4">
                            <!-- Post title-->
                            <h1 class="fw-bolder mb-1"><?php echo $post_title;?></h1>
                            <!-- Post meta content-->
                            <div class="text-muted fst-italic mb-2">Posted on <?php echo $post_date;?> by <?php echo $post_author;?></div>
                            <!-- Post categories-->
                            <a class="badge bg-secondary text-decoration-none link-light" href="*"><?php echo $post_tag;?></a>
                        </header>
                        <!-- Preview image figure-->
                        <figure class="mb-4"><img class="img-fluid rounded" src="./img/<?php echo $post_image;?>" alt="..." /></figure>
                        <!-- Post content-->
                        <section class="mb-5">
                            <p class="fs-5 mb-4"><?php echo $post_content;?></p>
                            <a type="button" href="./blog_details.php?post=<?php echo $post_id;?>" class="btn btn-outline-secondary">Continue Reading</a>
                        </section>
                    </article>

                    <?php } ?>


</div>
                <!-- Side widgets-->
                <?php include 'includes/sidebar.php'?>

<?php include'includes/footer.php';?>
