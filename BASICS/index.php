<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php basics</title>
</head>
<body>

     <!-- variable string number math -->
    <!-- <button>
        <?php
        $bag = 'click';
        echo $bag;
        ?>
    </button>
    <?php
    echo 1 +1 ;
    echo '1' + '1';
    echo 'isa' . 'kki';
    ?> -->
       <!-- arrays -->
<!-- <?php
        $list = [1 ,2 ,3];
        print_r($list);
        echo $list[0];
?> -->

           <!-- associative array -->
           <!-- <?php
                $list = ['<h1>HELLO</h1>'];
                print_r($list);
                echo $list[0];
           ?>



                <?php
                $someName = ['name' => 'isakki'];
                print_r($someName);
                echo '<br>';
                echo $someName['name'];
                ?> -->

                <!-- if else statement -->
<!-- 
                <?php
                $payment = true;
                if($payment){
                    echo'payment done';
                }

                else{
                    echo 'BUY A COURSE';
                }
                ?>

                <?php
                
                $user = 'asdf';

                if($user == 'admin'){
                    echo 'HELLO ADMIN';
                }

                elseif($user == 'student'){
                    echo 'HELLO STUDENT';
                }

                else{
                    echo 'HELLO ANONYMOUS';
                }
                ?> -->




                <!-- comparison  & logical operators -->
                 
                <!-- comparison operators 
                      ==
                      ===  it checks strings numbers alsoo...
                      <
                        >
                        >=
                        <=
                        !=
                -->

                <!-- LOGICAL OPERATOR
                &&  and same true..
                ||  or any one side
                -->



                <!-- SWITCH STATEMENTS -->

                <!-- <?php
                $users = 'teacher';

                switch($users) {

                    case 'admin':
                        echo 'hi admin';
                        break;
                    case 'teacher':
                        echo 'hi teacher';
                        break;
                }
                ?> -->




                <!-- WHILE LOOP -->

                <!-- <?php
                $number = 0;
                while($number <10){
                    echo $number;
                    echo '<br>';
                    // $number = $number +1;
                    $number++;
                }
                ?> -->




                <!-- FOR LOOP -->

                <!-- <?php


                for($number = 0; $number<10 ; $number++){
                    echo 'HI ISAKKI..';
                }
                
                
                ?> -->




                    <!-- for each loop -->



                <!-- <?php
                $numbers = [1,2,3,4,5,6,7,8,9,10];
                foreach($numbers as $number){
                    echo $number . '<br>';
                }
                ?>    -->




                <!--FUNCTIONS IN PHP -->

                <!-- <?php
                function some($name){
                    echo 'Hii..' . $name;
                }



                ?>
                <button>
                    <?php  some('isakki'); ?>
                </button> -->



                <!--RETURN VALUES IN FUNCTIONS...-->

            
                <!-- <?php
                function number($num1 , $num2){
                    return $num1 + $num2;
                }

                $result = number(10,20);
                echo $result;
                ?> -->


                <!--global variables in php-->

                <!-- <?php
                $number = 20;

                function convert(){
                    global $number;
                    $number = 30;
                }


                echo $number;
                echo '<br>';
                convert();
                echo $number;
                ?> -->

</body>
</html>