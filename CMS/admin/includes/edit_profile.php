<?php


    if(isset($_SESSION['email'])){

        $current_user = $_SESSION['email'];

        $select_all_query = "SELECT * FROM users WHERE user_email = '$current_user'";
        $select_all_result = mysqli_query($connection , $select_all_query);

        while($row = mysqli_fetch_assoc($select_all_result)){
            $username = $row['username'];
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $user_email = $row['user_email'];
            $user_password = $row['user_password'];
            $user_image = $row['user_image'];
        }
    }
    if(isset($_POST['update_user'])){
        $username = $_POST['username'];
        $user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $user_email = $_POST['user_email'];
        $user_image = $_FILES['image']['name'];
        $user_image_temp = $_FILES['image']['tmp_name'];
        $user_password = $_POST['user_password'];

        move_uploaded_file($user_image_temp , "../img/$user_image");


        if(empty($user_image)){
            $profile_query = "SELECT * FROM users WHERE user_email = '$current_user'";
            $search_result = mysqli_query($connection,$profile_query);

            while ($row = mysqli_fetch_assoc($search_result)){
                $user_image = $row['user_image'];
            }
        }





        $update_profile_query = "UPDATE users SET username = '$username' , user_firstname = '$user_firstname' , user_lastname = '$user_lastname' ,user_image = '$user_image' ,  user_email='$user_email' , user_password='$user_password'  WHERE user_email = '$current_user'";

        $update_profile_result = mysqli_query($connection , $update_profile_query);
        if(!$update_profile_result){
            die("QueryFailed");
        }
    }
?>

<div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit Profile</h6>
        </div>
        <div class="card-body">
            <form action="profile.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title">Username</label>
                    <input type="text" value="<?php echo $username;?>" class="form-control" name="username">
                </div>
                <div class="form-group">
                    <label for="title">Firstname</label>
                    <input type="text" value="<?php echo $user_firstname;?>" class="form-control" name="user_firstname">
                </div>
                <div class="form-group">
                    <label for="title">Lastname</label>
                    <input type="text" value="<?php echo $user_lastname;?>" class="form-control" name="user_lastname">
                </div>
                <div class="form-group">
                    <label for="title">Profile Pic</label> <br>
                    <img src="../img/<?php echo $user_image;?>" alt="" width="200px" srcset=""> <br> <br>
                    <input type="file" name="image">
                </div>
                <div class="form-group">
                    <label for="title">Email</label>
                    <input type="text" value="<?php echo $user_email;?>" class="form-control" name="user_email">
                </div>
                <div class="form-group">
                    <label for="title">Password</label>
                    <input type="password" value="<?php echo $user_password;?>" class="form-control" name="user_password">
                </div>
                <div class="form-group">
                    <input type="submit" value="Update User" class="btn btn-outline-primary" name="update_user">
                </div>
            </form>
        </div>
</div>