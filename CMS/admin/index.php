<?php
$connection = mysqli_connect('localhost', 'root', '', 'blog');

include 'includes/header.php';
include 'includes/sidebar.php';
include 'includes/nav.php'


?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">

        <h1 class="h3 mb-0 text-gray-800">Hello.. <br> Welcome back..<?php echo $_SESSION['username']; ?></h1>

        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Posts</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">

                                <?php

                                $post_query = "SELECT * FROM posts WHERE post_status = 'Published'";
                                $post_result = mysqli_query($connection, $post_query);
                                $post_count = mysqli_num_rows($post_result);
                                echo $post_count;

                                $post_un_query = "SELECT * FROM posts WHERE post_status = 'UnPublished'";
                                $post_un_result = mysqli_query($connection, $post_un_query);
                                $post_un_count = mysqli_num_rows($post_un_result);

                                ?>

                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">

                                Categories
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">


                                <?php

                                $category_query = "SELECT * FROM category";
                                $category_result = mysqli_query($connection, $category_query);
                                $category_count = mysqli_num_rows($category_result);
                                echo $category_count;

                                ?>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Users
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                        <?php

                                        $user_query = "SELECT * FROM users";
                                        $user_result = mysqli_query($connection, $user_query);
                                        $user_count = mysqli_num_rows($user_result);
                                        echo $user_count;

                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-auto">
                            <i class=" fas fa-solid fa-users fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Comments</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">

                                <?php

                                $comment_query = "SELECT * FROM comments WHERE comment_status = 'Approved'";
                                $comment_result = mysqli_query($connection, $comment_query);
                                $comment_count = mysqli_num_rows($comment_result);
                                echo $comment_count;

                                
                                $comment_un_query = "SELECT * FROM comments WHERE comment_status = 'UnApproved'";
                                $comment_un_result = mysqli_query($connection, $comment_un_query);
                                $comment_un_count = mysqli_num_rows($comment_un_result);

                                ?>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-7  col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                <script type="text/javascript">
                            google.charts.load('current', {
                                'packages': ['bar']
                            });
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                    ['data' , 'published' , 'UnPublished'],

                                    <?php
                                    
                                    echo "['Posts' , $post_count , $post_un_count],";
                                    echo "['Category' ,  $category_count , 0],";
                                    echo "['Users' , $user_count , 0],";
                                    echo "['Comments' , $comment_count , $comment_un_count]";

                                    ?>

                                ]);

                                var options = {
                                    chart: {
                                        title: 'Company Performance',
                                        subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                                    }
                                };

                                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        </script>
                        <div id="columnchart_material" style="width: 800px; height: 500px;"></div>
                </div>
            </div>
        </div>

        <!-- Pie Chart -->
        <div class="col-xl-5 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Revenue Sources</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="myPieChart"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                        <span class="mr-2">
                            <i class="fas fa-circle text-primary"></i> Direct
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-success"></i> Social
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-info"></i> Referral
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
<!-- Footer -->
<?php include 'includes/footer.php'; ?>