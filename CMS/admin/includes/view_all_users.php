<?php ob_start()
?>

<div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">View All Users</h6>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr> 
                        <th>Username</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Admin</th>
                        <th>Subscriber</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                <?php

                $select_all_users_query = "SELECT * FROM users";
                $select_all_users_result = mysqli_query($connection , $select_all_users_query);

                while($row = mysqli_fetch_assoc($select_all_users_result)){
                    $user_id = $row['user_id'];
                    $username = $row['username'];
                    $user_firstname = $row['user_firstname'];
                    $user_lastname = $row['user_lastname'];
                    $user_email = $row['user_email'];
                    $user_role = $row['user_role'];
                

                    echo "<tr>
                            <td>{$username}</td>
                            <td>{$user_firstname}</td>
                            <td>{$user_lastname}</td>
                            <td>{$user_email}</td>
                            <td>{$user_role}</td>
                            <td><a href='users.php?admin={$user_id}' class='btn btn-outline-success'>Admin</a></td>
                            <td><a href='users.php?sub={$user_id}' class='btn btn-outline-warning'>Subscriber</a></td>
                            <td><a href='users.php?delete={$user_id}' class='btn btn-outline-danger'>Delete</a></td>
                    </tr>"
                ?>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>


<?php
if(isset($_GET['delete'])){
    $delete_user_id = $_GET['delete'];
    $delete_user_query = "DELETE FROM users WHERE user_id = $delete_user_id";
    $delete_result = mysqli_query($connection , $delete_user_query);
    header('location: users.php');
}




if(isset($_GET['admin'])){
    $admin_user_id = $_GET['admin'];
    $admin_user_query = "UPDATE users SET user_role = 'Admin' WHERE user_id = $admin_user_id";
    $admin_user_result = mysqli_query($connection , $admin_user_query);
    header('location: users.php');
}




if(isset($_GET['sub'])){
    $sub_user_id = $_GET['sub'];
    $sub_user_query = "UPDATE users SET user_role = 'Subscriber' WHERE user_id = $sub_user_id";
    $sub_user_result = mysqli_query($connection , $sub_user_query);
    header('location: users.php');
}
?>